//
//  CustomNavigationViewController.m
//  KAVCKit
//
//  Created by Anton Kovalev on 01.04.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "CustomNavigationViewController.h"

@interface CustomNavigationViewController () <KANavigationControllerDelegate, KANavigationBarDelegate>

@end

@implementation CustomNavigationViewController

@dynamic navigationBar;

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self setDelegate:self];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (KANavigationBar *)navigationBarForNavigationController:(KANavigationController *)navigationController {
    return [[CustomNavigationBar alloc] init];
}

- (CGSize)navigationController:(KANavigationController *)navigationController sizeForNavigationBar:(KANavigationBar *)navigationBar {
    double height = 64;
    double width = CGRectGetWidth([UIScreen mainScreen].bounds);
    return CGSizeMake(width, height);
}

- (void)navigationBarBackButtonPressed:(KANavigationBar *)navigationBar {
    [self popViewControllerAnimated:YES completion:nil];
}

@end
