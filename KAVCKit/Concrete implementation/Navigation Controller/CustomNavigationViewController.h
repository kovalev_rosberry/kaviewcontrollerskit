//
//  CustomNavigationViewController.h
//  KAVCKit
//
//  Created by Anton Kovalev on 01.04.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "KANavigationController.h"
#import "CustomNavigationBar.h"

@interface CustomNavigationViewController : KANavigationController

@property (nonatomic, strong) CustomNavigationBar *navigationBar;

@end
