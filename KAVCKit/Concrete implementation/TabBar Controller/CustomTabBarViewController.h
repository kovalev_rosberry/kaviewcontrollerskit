//
//  CustomTabBarViewController.h
//  KAVCKit
//
//  Created by Anton Kovalev on 05.04.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "KATabBarController.h"

@interface CustomTabBarViewController : KATabBarController

@end
