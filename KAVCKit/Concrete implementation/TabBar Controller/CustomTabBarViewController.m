//
//  CustomTabBarViewController.m
//  KAVCKit
//
//  Created by Anton Kovalev on 05.04.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "CustomTabBarViewController.h"

@interface CustomTabBarViewController () <KATabBarControllerDelegate>

@end

@implementation CustomTabBarViewController

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self setDelegate:self];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

#pragma mark - KATabBarControllerDelegate

- (NSUInteger)ka_tabBarControllerNumberOfTabBars:(KATabBarController *)tabBarController {
    return 1;
}

- (KATabBar *)tabBarAtIndex:(NSUInteger)index {
    return [[KATabBar alloc] init];
}

- (UIButton *)ka_tabBarController:(KATabBarController *)tabBarController buttonForViewController:(UIViewController *)vc inTabBar:(KATabBar *)tabBar atIndex:(NSUInteger)index {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:vc.title forState:UIControlStateNormal];
    [button setBackgroundColor:[UIColor lightGrayColor]];
    return button;
}

- (KATabBarOrientation)ka_tabBarController:(KATabBarController *)tabBarController orientationForTabBar:(KATabBar *)tabBar {
    return KATabBarOrientationHorizontal;
}

- (CGRect)ka_tabBarController:(KATabBarController *)tabBarController frameForTabBar:(KATabBar *)tabBar {
    return [self tabBarFrame];
}

- (CGRect)ka_tabBarController:(KATabBarController *)tabBarController frameForControllersContainerView:(UIView *)controllersContainerView {
    CGRect tabBarFrame = [self tabBarFrame];
    return CGRectMake(0, 0, CGRectGetWidth(tabBarFrame), CGRectGetMinY(tabBarFrame));
}

- (CGRect)tabBarFrame {
    double width = CGRectGetWidth(self.view.bounds);
    double height = 49;
    double y = CGRectGetHeight(self.view.bounds) - height;
    return CGRectMake(0, y, width, height);
}

@end
