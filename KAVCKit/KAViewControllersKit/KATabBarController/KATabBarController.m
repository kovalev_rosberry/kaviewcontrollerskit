//
//  CustomTabBarViewController.m
//  Q-Less vendor
//
//  Created by Anton on 08.10.14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "KATabBarController.h"
#import "KAViewControllersKit.h"
#import "KATabBar.h"
#import <objc/runtime.h>
#import "KAPrivateTransitionContext.h"
#import "KATabBarControllerControllersContainerView.h"
#import "KATabBarControllerCommonContainerView.h"
#import "KAPrivateNavigationController.h"


@interface KATabBarControllerPrivateAnimatedTransition : NSObject <UIViewControllerAnimatedTransitioning>
@end


@interface KATabBarController () <KATabBarDelegate>

@property (nonatomic) KATabBarControllerCommonContainerView *commonContainerView;

@property (nonatomic) NSMutableArray *tempStoryboardViewControllersArray;
@property (nonatomic) NSString *localTitle;

@property (nonatomic, strong, readwrite) NSArray *tabBars;
@property (nonatomic, weak, readwrite) KATabBar *activeTabBar;
@property (nonatomic, strong, readwrite) KATabBarControllerControllersContainerView *viewControllersContainerView;

@property (nonatomic) UIViewController *zeroStateViewController;
@property (nonatomic) NSString *settedTitle;

@end

@implementation KATabBarController

- (void)dealloc {
    self.viewControllersContainerView = nil;
    self.tabBars = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.commonContainerView = [[KATabBarControllerCommonContainerView alloc] init];
    [self.view addSubview:self.commonContainerView];
    
    self.viewControllersContainerView = [[KATabBarControllerControllersContainerView alloc] init];
    [self.commonContainerView addSubview:self.viewControllersContainerView];
    
    NSMutableArray *tabBars = [NSMutableArray array];
    NSInteger numberOfTabBars = [self numberOfTabBars];
    for (int i = 0; i < numberOfTabBars; ++i) {
        KATabBar *tabBar = [self tabBarAtIndex:i];
        if (tabBar) {
            [self.commonContainerView addSubview:tabBar];
            [tabBar setIndex:i];
            [tabBar setDelegate:self];
            [tabBars addObject:tabBar];
            
            [tabBar layoutIfNeeded];
        }
    }
    self.tabBars = [NSArray arrayWithArray:tabBars];
    
    if ([self.delegate respondsToSelector:@selector(ka_tabBarController:performAdditionalConfigurationsForCommonContainerView:)]) {
        [self.delegate ka_tabBarController:self performAdditionalConfigurationsForCommonContainerView:self.commonContainerView];
    }
    
    [self loadViewControllersFromStoryBoard];
    
    for (KATabBar *tabBar in self.tabBars) {
        if (tabBar.viewControllers.count == 0)
            [tabBar setViewControllers:[self viewControllersForTabBar:tabBar]];
    }
    
    KATabBar *tabBar = self.tabBars.firstObject;
    [self setShouldNotPerformTransitions:YES];
    [tabBar setSelectedIndex:0];
    [self setShouldNotPerformTransitions:NO];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    [self.commonContainerView setFrame:self.view.bounds];
    
    CGRect controllersContainerViewFrame = [self.delegate ka_tabBarController:self frameForControllersContainerView:self.viewControllersContainerView];
    [self.viewControllersContainerView setFrame:controllersContainerViewFrame];
    
    for (int i = 0; i < self.tabBars.count; ++i) {
        KATabBar *tabBar = self.tabBars[i];
        if (tabBar) {
            CGRect tabBarFrame = [self.delegate ka_tabBarController:self frameForTabBar:tabBar];
            [tabBar setFrame:tabBarFrame];
            
            [tabBar layoutIfNeeded];
        }
    }
}

- (void)beginAppearanceTransition:(BOOL)isAppearing animated:(BOOL)animated {
    [super beginAppearanceTransition:isAppearing animated:animated];
    [self.activeTabBar.selectedViewController beginAppearanceTransition:isAppearing animated:animated];
}

- (void)endAppearanceTransition {
    [super endAppearanceTransition];
    [self.activeTabBar.selectedViewController endAppearanceTransition];
}

#pragma mark - Override

- (void)setTitle:(NSString *)title {
    self.settedTitle = title;
}

- (NSString *)title {
    if (!self.settedTitle) {
        return self.activeTabBar.selectedViewController.title;
    }
    return self.settedTitle;
}

#pragma mark - Public methods

- (void)showViewControllerInZeroState:(UIViewController *)vc animated:(BOOL)animated {
    if ([self.delegate respondsToSelector:@selector(ka_tabBarController:willShowViewControllerInZeroState:)]) {
        [self.delegate ka_tabBarController:self willShowViewControllerInZeroState:vc];
    }
    
    if (self.zeroStateViewController) {
        [self startHidingZeroStateViewController];
        [self endHidingZeroStateViewController];
    } else {
        [self tabBarShouldHideSelectedViewController:self.activeTabBar];
        [self.activeTabBar resetSelection];
    }
    
    UIView *view = vc.view;
    [vc beginAppearanceTransition:YES animated:animated];
    
    [self.viewControllersContainerView insertSubview:view atIndex:NSIntegerMax];
    [view setFrame:self.viewControllersContainerView.bounds];
    
    [vc didMoveToParentViewController:self];
    [vc endAppearanceTransition];
    
    vc.ka_tabBarController = self;
    self.zeroStateViewController = vc;
    
    if ([self.delegate respondsToSelector:@selector(ka_tabBarController:didShowViewControllerInZeroState:)]) {
        [self.delegate ka_tabBarController:self didShowViewControllerInZeroState:vc];
    }
}

- (BOOL)isInZeroState {
    return self.zeroStateViewController != nil;
}

#pragma mark - Helper Methods

- (void)startHidingZeroStateViewController {
    if ([self.delegate respondsToSelector:@selector(ka_tabBarController:willHideViewControllerInZeroState:)]) {
        [self.delegate ka_tabBarController:self willHideViewControllerInZeroState:self.zeroStateViewController];
    }
    
    [self.zeroStateViewController beginAppearanceTransition:NO animated:NO];
    [self.zeroStateViewController willMoveToParentViewController:nil];
}

- (void)endHidingZeroStateViewController {
    [self.zeroStateViewController.view removeFromSuperview];
    [self.zeroStateViewController removeFromParentViewController];
    [self.zeroStateViewController endAppearanceTransition];
    
    if ([self.delegate respondsToSelector:@selector(ka_tabBarController:didHideViewControllerInZeroState:)]) {
        [self.delegate ka_tabBarController:self didHideViewControllerInZeroState:self.zeroStateViewController];
    }
    
    self.zeroStateViewController = nil;
}

- (void)didSelectCurrentViewControllerInTabBar:(KATabBar *)tabBar {
    UIViewController *selectedViewController = tabBar.selectedViewController;
    if ([selectedViewController isKindOfClass:[KANavigationController class]]) {
        KANavigationController *nav = (id)selectedViewController;
        [self handleSameTabSelectionForNavigationController:nav];
    } else if ([selectedViewController isKindOfClass:[KATabBarController class]]) {
        KATabBarController *innerTabbarController = (id)selectedViewController;
        UIViewController *innerSelectedViewController = innerTabbarController.activeTabBar.selectedViewController;
        if ([innerSelectedViewController isKindOfClass:[KANavigationController class]]) {
            KANavigationController *nav = (id)innerSelectedViewController;
            [self handleSameTabSelectionForNavigationController:nav];
        }
    } else {
        [self handlePossibleScrollUpForCustomViewController:selectedViewController];
    }
}

- (void)handleSameTabSelectionForNavigationController:(KANavigationController *)nav {
    UIViewController *topViewController = nav.topViewController;
    if ([topViewController isEqual:nav.rootViewController]) {
        [self handlePossibleScrollUpForCustomViewController:topViewController];
    } else {
        [nav popToRootViewControllerAnimated:YES completion:nil];
    }
}

- (void)handlePossibleScrollUpForCustomViewController:(UIViewController *)vc {
    if ([vc isKindOfClass:[KATabBarController class]]) {
        KATabBarController *tabBarVC = (id)vc;
        vc = (id)tabBarVC.activeTabBar.selectedViewController;
    }
    for (UIView *sv in vc.view.subviews) {
        if ([sv isKindOfClass:[UIScrollView class]]) {
            UIScrollView *scrollView = (id)sv;
            CGPoint zeroPoint = CGPointMake(0, 0 - scrollView.contentInset.top);
            if (!CGPointEqualToPoint(scrollView.contentOffset, zeroPoint)) {
                if ([self.delegate respondsToSelector:@selector(ka_tabBarController:willScrollScrollView:toZeroPoint:inViewController:)]) {
                    [self.delegate ka_tabBarController:self willScrollScrollView:scrollView toZeroPoint:zeroPoint inViewController:vc];
                }
                
                [scrollView setContentOffset:zeroPoint animated:YES];
            }
        }
    }
}

#pragma mark - Storyboard checking

- (void)loadViewControllersFromStoryBoard {
    for (int i = 0; i < self.tabBars.count; ++i) {
        self.tempStoryboardViewControllersArray = [NSMutableArray array];
        
        for (int j = 0; j < HUGE; ++j) {
            NSString *segueIdentifier = [NSString stringWithFormat:@"%li%@%li", (long)i, customSegueIdentifierBaseString, (long)j];
            if ([self canPerformSegueWithIdentifier:segueIdentifier]) {
                [self performSegueWithIdentifier:segueIdentifier sender:self];
            } else {
                break;
            }
        }
        
        KATabBar *tabBar = self.tabBars[i];
        [tabBar setViewControllers:[NSArray arrayWithArray:self.tempStoryboardViewControllersArray]];
        self.tempStoryboardViewControllersArray = nil;
    }
}

- (void)addViewControllerFromStoryboard:(UIViewController *)vc {
    vc.ka_tabBarController = self;
    [self.tempStoryboardViewControllersArray addObject:vc];
}

#pragma mark - Forwarding Appearance Methods

- (BOOL)shouldAutomaticallyForwardAppearanceMethods {
    return NO;
}

#pragma mark - Delegate

- (KATabBar *)tabBarAtIndex:(NSUInteger)index {
    if ([self.delegate respondsToSelector:@selector(tabBarAtIndex:)])
        return [self.delegate tabBarAtIndex:index];
    return [[KATabBar alloc] init];
}

- (NSInteger)numberOfTabBars {
    if ([self.delegate respondsToSelector:@selector(ka_tabBarControllerNumberOfTabBars:)])
        return [self.delegate ka_tabBarControllerNumberOfTabBars:self];
    return 1;
}

- (NSArray *)viewControllersForTabBar:(KATabBar *)tabBar {
    if ([self.delegate respondsToSelector:@selector(ka_tabBarController:viewControllersForTabBar:)])
        return [self.delegate ka_tabBarController:self viewControllersForTabBar:tabBar];
    return nil;
}

#pragma mark - KATabBarDelegate

- (void)tabBarShouldHideSelectedViewController:(KATabBar *)tabBar {
    UIViewController *vc = self.activeTabBar.selectedViewController;
    if (!self.shouldNotPerformTransitions)
        [vc beginAppearanceTransition:NO animated:NO];
    [self removeSelectedViewControllersInAllTabBarsExcept:tabBar];
    [vc willMoveToParentViewController:nil];
    [vc.view removeFromSuperview];
    [vc removeFromParentViewController];
    if (!self.shouldNotPerformTransitions)
        [vc endAppearanceTransition];
    [tabBar setSelectedViewController:nil];
}

- (void)tabBar:(KATabBar *)tabBar shouldCycleToViewController:(UIViewController *)toViewController animated:(BOOL)animated force:(BOOL)force changeSelections:(void (^)())change completion:(void (^)(BOOL))completion{
    [self.activeTabBar resetSelection];
    self.activeTabBar = tabBar;
    
    UIViewController *fromViewController = self.activeTabBar.selectedViewController;
    
    if (self.zeroStateViewController) {
        [self startHidingZeroStateViewController];
    }
    
    if (self.ka_navigationController) {
        [self.ka_navigationController ka_tabBarControllerDidStartTransition:self from:fromViewController to:toViewController];
    }
    
    if (!self.shouldNotPerformTransitions) {
        [fromViewController beginAppearanceTransition:NO animated:animated];
        if (change) {
            change();
        }
        [toViewController view];
        [toViewController beginAppearanceTransition:YES animated:animated];
    } else {
        if (change) {
            change();
        }
    }
    [fromViewController willMoveToParentViewController:nil];
    [self addChildViewController:toViewController];
    
    id<UIViewControllerAnimatedTransitioning> animator = nil;
    if ([self.delegate respondsToSelector:@selector(tabBarController:animationControllerForTransitionFromViewController:toViewController:)])
        animator = [self.delegate tabBarController:self animationControllerForTransitionFromViewController:fromViewController toViewController:toViewController];
    if(!animator) {
        animator = (id)[[KATabBarControllerPrivateAnimatedTransition alloc] init];
    }
    
    BOOL isAnimatorDefault = [animator isKindOfClass:[KATabBarControllerPrivateAnimatedTransition class]];
    
    __weak typeof(self) weakSelf = self;
    KAPrivateTransitionContext *context = [[KAPrivateTransitionContext alloc] initWithFromViewController:fromViewController toViewController:toViewController tabBarController:self];
    if (!self.shouldNotPerformTransitions) {        
        if (!animated && !force) {
            animated = !isAnimatorDefault;
        }
    }
    [context setAnimated:animated];
    [context setCompletionBlock:^(BOOL finished) {
        if (finished) {
            [fromViewController.view removeFromSuperview];
            [fromViewController removeFromParentViewController];
            if (!weakSelf.shouldNotPerformTransitions) {
                [fromViewController endAppearanceTransition];
                [toViewController endAppearanceTransition];
            }
            [toViewController didMoveToParentViewController:weakSelf];
            [weakSelf removeSelectedViewControllersInAllTabBarsExcept:tabBar];
            
            if (weakSelf.zeroStateViewController) {
                [weakSelf endHidingZeroStateViewController];
            }
            
            if (weakSelf.ka_navigationController) {
                [weakSelf.ka_navigationController ka_tabBarControllerDidFinishTransition:weakSelf from:fromViewController to:toViewController];
            }
        }
        else {
            if (weakSelf.ka_navigationController) {
                [weakSelf.ka_navigationController ka_tabBarControllerDidCancelTransition:weakSelf from:fromViewController to:toViewController];
            }
        }
        if (completion) {
            completion(finished);
        }
    }];
    
    id<UIViewControllerInteractiveTransitioning> interactionController = [self interactionControllerForAnimator:animator animatorIsDefault:isAnimatorDefault];
    
    [context setInteractive:(interactionController != nil)];
    
    if ([context isInteractive]) {
        [interactionController startInteractiveTransition:context];
    } else {
        [animator animateTransition:context];
    }
}

- (id<UIViewControllerInteractiveTransitioning>)interactionControllerForAnimator:(id<UIViewControllerAnimatedTransitioning>)animationController animatorIsDefault:(BOOL)animatorIsDefault {
    return nil;
}

- (void)removeSelectedViewControllersInAllTabBarsExcept:(KATabBar *)tabBar {
    for (KATabBar *t in self.tabBars)
        if (![t isEqual:tabBar])
            [t setSelectedViewController:nil];
}

- (void)tabBar:(KATabBar *)tabBar didSelectViewController:(UIViewController *)vc {
    if ([self.delegate respondsToSelector:@selector(ka_tabBarController:didSelectViewController:inTabBar:)]) {
        [self.delegate ka_tabBarController:self didSelectViewController:vc inTabBar:tabBar];
    }
}

- (KATabBarOrientation)tabBarOrientation:(KATabBar *)tabBar {
    if ([self.delegate respondsToSelector:@selector(ka_tabBarController:orientationForTabBar:)]) {
        return [self.delegate ka_tabBarController:self orientationForTabBar:tabBar];
    }
    return KATabBarOrientationHorizontal;
}

- (UIButton *)tabBar:(KATabBar *)tabBar buttonForViewController:(UIViewController *)vc atIndex:(NSUInteger)index {
    if ([self.delegate respondsToSelector:@selector(ka_tabBarController:buttonForViewController:inTabBar:atIndex:)]) {
        return [self.delegate ka_tabBarController:self buttonForViewController:vc inTabBar:tabBar atIndex:index];
    }
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:vc.title forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor colorWithRed:0.82 green:0.42 blue:0.33 alpha:1] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor colorWithRed:0.25 green:0.84 blue:0.46 alpha:1] forState:UIControlStateSelected];
    [btn setTitleColor:[UIColor colorWithRed:0.25 green:0.84 blue:0.46 alpha:1] forState:UIControlStateSelected|UIControlStateHighlighted];
    
    [vc setTabBarButton:btn];
    
    return btn;
}

- (BOOL)tabBar:(KATabBar *)tabBar shouldSelectViewController:(UIViewController *)vc {
    if ([self.delegate respondsToSelector:@selector(ka_tabBarController:shouldSelectViewController:inTabBar:)]) {
        return [self.delegate ka_tabBarController:self shouldSelectViewController:vc inTabBar:tabBar];
    }
    return YES;
}

- (UIColor *)tabBarButtonsDelimeterColor:(KATabBar *)tabBar {
    if ([self.delegate respondsToSelector:@selector(ka_tabBarController:delimeterColorForTabBar:)]) {
        return [self.delegate ka_tabBarController:self delimeterColorForTabBar:tabBar];
    }
    return [UIColor blackColor];
}

- (double)tabBarDelimeterWidth:(KATabBar *)tabBar {
    if ([self.delegate respondsToSelector:@selector(ka_tabBarController:delimeterWidthForTabBar:)]) {
        return [self.delegate ka_tabBarController:self delimeterWidthForTabBar:tabBar];
    }
    return 0;
}

- (void)tabBarDidSelectCurrentViewController:(KATabBar *)tabBar {
    [self didSelectCurrentViewControllerInTabBar:tabBar];
    
    if ([self.delegate respondsToSelector:@selector(ka_tabBarController:didSelectCurrentViewControllerInTabBar:)]) {
        [self.delegate ka_tabBarController:self didSelectCurrentViewControllerInTabBar:tabBar];
    }
}

@end


@implementation UIViewController (CustomTabBarController)

- (void)setKa_tabBarController:(KATabBarController *)ka_tabBarController {
    objc_setAssociatedObject(self, @selector(setKa_tabBarController:), ka_tabBarController, OBJC_ASSOCIATION_ASSIGN);
}

- (KATabBarController *)ka_tabBarController {
    return objc_getAssociatedObject(self, @selector(setKa_tabBarController:));
}

- (void)setTabBarButton:(UIButton *)tabBarButton {
    objc_setAssociatedObject(self, @selector(setTabBarButton:), tabBarButton, OBJC_ASSOCIATION_ASSIGN);
}

- (UIButton *)tabBarButton {
    return objc_getAssociatedObject(self, @selector(setTabBarButton:));
}

- (UIImage *)tabBarImageForState:(UIControlState)state {
    return nil;
}

@end


#pragma mark - CUSTOM SEGUE

NSString* const customSegueIdentifierBaseString = @"CTVC";
@implementation CustomTabBarSetViewControllerSegue

- (void)perform {
    KATabBarController *svc = self.sourceViewController;
    UIViewController *dvc = self.destinationViewController;
    
    [svc addViewControllerFromStoryboard:dvc];
}

@end


#pragma mark - PRIVATE ANIMATED TRANSITION

@implementation KATabBarControllerPrivateAnimatedTransition

- (NSTimeInterval)transitionDuration:(id<KAViewControllerContextTransitioning>)transitionContext {
    return .45f;
}

- (void)animateTransition:(id<KAViewControllerContextTransitioning>)transitionContext{
    UIViewController *toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    [transitionContext startTransitionWithDuration:[self transitionDuration:transitionContext]];
    
    UIView *containerView = [transitionContext containerView];
    UIView *nview = toViewController.view;
    
    [containerView insertSubview:nview atIndex:NSIntegerMax];
    
    if (transitionContext.isAnimated) {
        [UIView animateWithDuration:[self transitionDuration:transitionContext] delay:.0f options:0x00 animations:^{
            [nview setFrame:containerView.bounds];
        } completion:^(BOOL finished) {
            [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
        }];
    } else {
        [nview setFrame:containerView.bounds];
        [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
    }
}

- (void)dealloc {
    
}

@end
