//
//  KANavigationControllerTransitionType.h
//  Collaborate
//
//  Created by Anton Kovalev on 03.03.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>

///Represent type of transition for navigation controller related views
typedef NS_ENUM(NSUInteger, KANavigationControllerTransitionType) {
    ///Type for push action
    KANavigationControllerTransitionTypePush,
    ///Type for pop actoin
    KANavigationControllerTransitionTypePop,
    ///Type for selecting tabs via tab bar action
    KANavigationControllerTransitionTypeTabBar
};
