//
//  ViewController.m
//  KAVCKit
//
//  Created by Anton Kovalev on 01.04.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "FirstViewController.h"
#import "KAViewControllersKit.h"

@interface FirstViewController ()

@end

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setTranslucentNavigationBar:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.ka_navigationController setNavigationBarHidden:YES animated:NO];
}

@end
